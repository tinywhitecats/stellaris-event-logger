

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileWriter;
import java.io.IOException;
import java.util.HashSet;
import java.util.Scanner;

public class gooberland {
	final static String PATH = "E:\\Program Files (x86)\\Steam\\steamapps\\workshop\\content\\281990";
	//final static String PATH = "E:\\Users\\tinywhitecat\\Documents\\Paradox Interactive\\Stellaris\\mod";
	final static boolean doRemove = false;
	final static boolean testObject = false;
	final static boolean skipWrite = false;
	static long count = 0;
	public static void main(String[] args) throws FileNotFoundException {
		searchDirectory(new File(PATH));
	}
	static void searchDirectory(File d) throws FileNotFoundException {
		if (!testObject) {
			for (File modDir : d.listFiles()) {
				System.out.println(modDir.getName());
				try {
					File eventsDir = new File(modDir.getAbsolutePath() + "/events");
					for (File eventFile : eventsDir.listFiles()) {
						checkFile(eventFile);
					}
				} catch (Exception e) {
					System.out.println("--Exception " + e);
				}
	    	}
			System.out.println("Found " + count + " events.");
		} else {
			try {
				//File eventsDir = new File(PATH + "/937289339/events");
				File eventsDir = new File("E:\\Program Files (x86)\\Steam\\steamapps\\common\\Stellaris\\events");
				for (File eventFile : eventsDir.listFiles()) {
					checkFile(eventFile);
				}
			} catch (Exception e) {
				System.out.println("--Exception " + e);
			}
		}
	}
	static void checkFile(File eventFile) throws FileNotFoundException {
		Scanner scanner = new Scanner(eventFile);
		System.out.println("-" + eventFile.getName());
		StringBuilder build = new StringBuilder();
		if (doRemove) {
		    while (scanner.hasNextLine()) {
		        String line = scanner.nextLine();
		        if (line.contains("#TWC_GOOBER_WAS_HERE_TOO"))
		        	build.append("}\n");
		        else if (!line.contains("#TWC_GOOBER_WAS_HERE"))
		        	build.append(line).append("\n");
		    }
		} else {
		    while (scanner.hasNextLine()) {
		        String line = scanner.nextLine();
		        build.append(line).append("\n");
		        if (line.contains("_event") || line.startsWith("event")) {
		        	//possible match
		        	  if (line.startsWith("event=") || line.startsWith("event =")
		      	        	|| line.startsWith("country_event =") || line.startsWith("country_event=")
		      	        	|| line.startsWith("planet_event =") || line.startsWith("planet_event=")
		      	        	|| line.startsWith("fleet_event =") || line.startsWith("fleet_event=")
		      	        	|| line.startsWith("ship_event =") || line.startsWith("ship_event=")
		      	        	|| line.startsWith("pop_faction_event =") || line.startsWith("pop_faction_event=")
		      	        	|| line.startsWith("pop_event =") || line.startsWith("pop_event=")
		      	        	|| line.startsWith("system_event =") || line.startsWith("system_event=")
		      	        	|| line.startsWith("starbase_event =") || line.startsWith("starbase_event=")
		      	        	|| line.startsWith("leader_event =") || line.startsWith("leader_event=")
		      	        	|| line.startsWith("espionage_operation_event =") || line.startsWith("espionage_operation_event=")
		      	        	|| line.startsWith("first_contact_event =") || line.startsWith("first_contact_event=")
		      	        	|| line.startsWith("agreement_event =") || line.startsWith("agreement_event=")
		      	        		) {
		        		  boolean foundImmediate = false;
		        		  while (scanner.hasNextLine()) {
		        		        String l2 = scanner.nextLine();
		        		        build.append(l2).append("\n");
		        		        String l2Clean = l2.replaceAll("\\s+","").trim();
		        		        if (l2Clean.startsWith("id=")) {
		        		        	System.out.println("--Event " + l2Clean);
		        		        	count++;
		        		        }
		        		        if (l2Clean.startsWith("immediate={")) {
		        		        	System.out.println("---immediate " + l2Clean);
		        		        	foundImmediate = true;
		        		        	if (!skipWrite & !doRemove)
		        		        		build.append("log = \"Fired immediate for [This.GetName].)\" #TWC_GOOBER_WAS_HERE").append("\n");;
		        		        	//add log
		        		        	//		log = "Fired immediate for [This.GetName]."
		        		        }
		        		        if (l2.equals("}")) {
		        		        	if (!foundImmediate) {
		        		        		//make new immediate for log
		        		        		System.out.println("---No immediate!");
		        		        		if (!skipWrite & !doRemove) {
		        		        			build.delete(build.length() - 2, build.length());
			        		        		build.append("immediate={log = \"Fired new immediate for [This.GetName].)\"}} #TWC_GOOBER_WAS_HERE_TOO").append("\n");;
		        		        		}
		        		        	}
		        		        	break;
		        		        }
		        		  }
		        	  }
		        }
		    }
	    }
	    scanner.close();
	    //overwrite
	    if (!skipWrite) {
		    BufferedWriter writer = null;
		    try {
		        writer = new BufferedWriter(new FileWriter(eventFile));
		        writer.append(build);
		    } catch (Exception e) {
		    	System.out.println("UNINTENDED ERROR 1: " + e);
		    } finally {
		        if (writer != null)
					try {
						writer.close();
					} catch (IOException e) {
						System.out.println("UNINTENDED ERROR 2: " + e);
					}
		    }
	    }
	}
}
